/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"

/* USER CODE BEGIN Includes */
#include "my_header.h"
#include "stm32f3xx_hal_gpio.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

uint8_t data[20];

uint8_t rx_index = 0;
uint8_t rx_data;
uint8_t rx_buffer[20];
int listen = 0; //Toggles between listening mode
int count = 1; //keeps track of data being entered in buffer
uint8_t newtemp[10]; //Hold temperature data
int tempSize = 0; //keeps track of the size of the temp entered





int j = 0;
int channel = 8; //Switch between channel 8 and 11

uint32_t adcValue[40];

uint32_t adcCurrentValue[40];
uint32_t adcVoltageValue[40];

float Irms;
float Vrms;

float currents[40];
float voltages[40];

float ambientTemp;
float waterTemp;

unsigned adc_raw;
unsigned int adcVal;


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	received = true;
	  //Read characters in until <LF> character is received
	  /*if (listen == 1)
	  {
		  //Put character into data buffer
		  data[count] = rx_data;
		  //increase count
		  count++;
		  //Prime UART
		  //HAL_UART_Receive_IT (&huart1, &rx_data, 1);
	  }

	  //When the last character is received
	  if (rx_data == 0xD) {
		  //Keep track of temp related characters
		  tempSize = count;

		  //Add the <LF> character to the data buffer and reset count
		  data[count] = 0xA;
		  count = 1;

		  //Do nothing until valid argument is entered
		  listen = 0;

		  //Resolve data entered
		  resolveData();
	  }

	  //listen for start of valid argument
	  if (rx_data == '$'){
		  //Activate mode to add new characters
		  listen = 1;
		  data[0] = '$';

	  }

	  //prime UART to receive another character
	  HAL_UART_Receive_IT (&huart1, &rx_data, 1);*/

}

void HAL_SYSTICK_Callback(void)
{
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_SYSTICK_Callback could be implemented in the user file
   */
	  /*if (counter == 1) {

		  //writeToDigit(1, '0');
		  counter++;
	  }
	  else if (counter == 2)	  {

		  switch(tempCount){
		  case 8:
			  writeToDigit(2, currentTemp[0]);
			  break;
		  case 7:
			  //writeToDigit(2, currentTemp[0]);
			  break;
		  case 6:
			  //writeToDigit(2, currentTemp[0]);
			  break;
		  }


		  counter++;
	  }
	  else if (counter == 3)
	  {

		  switch(tempCount){
		  case 8:
			  writeToDigit(3, currentTemp[1]);
			  break;
		  case 7:
			  writeToDigit(3, currentTemp[0]);
			  break;
		  case 6:
			  break;
		  }

		  //writeToDigit(3, currentTemp[1] );
		  counter++;
	  }
	  else
	  {

		  switch(tempCount){
		  case 8:
			  writeToDigit(4, currentTemp[2]);
			  break;
		  case 7:
			  writeToDigit(4, currentTemp[1]);
			  break;
		  case 6:
			  writeToDigit(4, currentTemp[0]);
			  break;
		  }
		  //writeToDigit(4, currentTemp[2]);
		  counter = 1;
	  }*/

	  //HAL_ADC_Start_IT(&hadc1);

	  //HAL_ADC_Start(&hadc1);
	  //sampleADC();

	  sysTickFlag = 1;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{

	adcTick = true; //Set flag for the completion of the adc conversion
	sampleADC();
	/*if(__HAL_ADC_GET_FLAG(&hadc1, ADC_FLAG_EOC)){

		//Get the sampled value
		adc_raw = HAL_ADC_GetValue(&hadc1);

		//Check current channel
		if (channel == 8)
		{
			//Add sampled value to voltages array
			adcVoltageValue[j] = adc_raw;

			//Add to float vale [Test this before removing uint vals]
			voltages[j] = adc_raw;

			//j++;

			//Reset counter and switch channels
			switchAdcChannel(11);
			channel = 11;


			if (j == 19) {
				//adcstuff();
				j = 0;
				//channel = 11;
			}
		}
		else if (channel == 11)
		{
			//Add sampled value to currents array
			adcCurrentValue[j] = adc_raw;

			//Add to float vale [Test this before removing uint vals]
			currents[j] = adc_raw;

			j++;

			//Reset counter and switch channels

			switchAdcChannel(8);
			channel = 8;

			if (j == 19) {
				//MX_ADC1_Init();
				j = 0;
				//channel = 8;
			}
		}

		//adcValue[j] = adc_raw;
		//j++;  //Increase the counter
		HAL_ADC_Start_IT(&hadc1);
	}*/
}



/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */



  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();

  /* USER CODE BEGIN 2 */
  //__HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE); //Enables receive interrupt

  initialiseUser(); //initialise the user variables

  //Prime the UART to receive data in interrupt mode
  HAL_UART_Receive_IT(&huart1, &rx_data, 1);

  //Start ADC interrupt
  //HAL_ADC_Start_IT(&hadc1);

  //HAL_ADC_Start(&hadc1);




  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  userloop();
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */






	  //HAL_Delay(1);
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV2;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_MultiModeTypeDef multimode;
  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the ADC multi-mode 
    */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_DOWN;
  htim2.Init.Period = 16000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
     PA2   ------> USART2_TX
     PA3   ------> USART2_RX
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8 
                          |GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PC0 PC1 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA4 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA2 PA3 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA5 PA6 PA7 PA8 
                           PA9 */
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8 
                          |GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB10 PB13 PB14 PB3 
                           PB4 PB5 PB6 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

//process the received data

void receivedata (void) {
	  //Read characters in until <LF> character is received
	  if (listen == 1)
	  {
		  //Put character into data buffer
		  data[count] = rx_data;
		  //increase count
		  count++;
		  //Prime UART
		  //HAL_UART_Receive_IT (&huart1, &rx_data, 1);
	  }

	  //When the last character is received
	  if (rx_data == 0xD) {
		  //Keep track of temp related characters
		  tempSize = count;

		  //Add the <LF> character to the data buffer and reset count
		  data[count] = 0xA;
		  count = 1;

		  //Do nothing until valid argument is entered
		  listen = 0;

		  //Resolve data entered
		  resolveData();
	  }

	  //listen for start of valid argument
	  if (rx_data == '$'){
		  //Activate mode to add new characters
		  listen = 1;
		  data[0] = '$';

	  }

	  received = false;
	  //prime UART to receive another character
	  HAL_UART_Receive_IT (&huart1, &rx_data, 1);
}
//Get the sampled values from the ADC
void sampleADC(void) {

		//Get the sampled value
	//HAL_ADC_Start(&hadc1);
	//HAL_ADC_PollForConversion(&hadc1, 5);
		//adc_raw = HAL_ADC_GetValue(&hadc1);

		//Check current channel
		if (channel == 8) //Voltage
		{
			//Add sampled value to voltages array
			//adcVoltageValue[j] = adc_raw;



			//j++;

			//Reset counter and switch channels


			//HAL_ADC_PollForConversion(&hadc1, 5);
			voltages[j] = HAL_ADC_GetValue(&hadc1);
			switchAdcChannel(9);
			channel = 9;


			//Add to float vale [Test this before removing uint vals]
			//voltages[j] = adc_raw;

		}
		else if (channel == 9) //Current
		{
			//Add sampled value to currents array
			//adcCurrentValue[j] = adc_raw;



			//Reset counter and switch channels



			//HAL_ADC_PollForConversion(&hadc1, 5);
			currents[j] = HAL_ADC_GetValue(&hadc1);
			switchAdcChannel(11);
			channel = 11;

			//Add to float vale [Test this before removing uint vals]
			//currents[j] = adc_raw;

			j++;

			/*if (j == 19) {
				//MX_ADC1_Init();
				j = 0;
				//channel = 8;
			}*/
		}
		else if (channel == 11) //Water temperature
		{




			//HAL_ADC_PollForConversion(&hadc1, 5);
			waterTemp = HAL_ADC_GetValue(&hadc1);
			switchAdcChannel(12);
			channel = 12;

			//Store the temperature value here
			//waterTemp = adc_raw;

		}
		else if (channel == 12) //Ambient temperature
		{





			//HAL_ADC_PollForConversion(&hadc1, 5);
			ambientTemp = HAL_ADC_GetValue(&hadc1);

			switchAdcChannel(8);
			channel = 8;


			//Store the temperature value here
			//ambientTemp = adc_raw;
		}

		//adcValue[j] = adc_raw;

		//Check if last sample has been done
		if (j == 40) {
			//Calculate Irms and Vrms
			Irms = calcRMS(currents, 40);
			Vrms = calcRMS(voltages, 40);
			j = 0;
		}

		//HAL_ADC_Start(&hadc1);

}

//Switch between the different ADC channels
void switchAdcChannel(int chann){
	  ADC_MultiModeTypeDef multimode;
	  ADC_ChannelConfTypeDef chdef;

	    /**Configure the ADC multi-mode
	    */
	  multimode.Mode = ADC_MODE_INDEPENDENT;
	  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
	  {
	    _Error_Handler(__FILE__, __LINE__);
	  }

	  switch (chann){
	  case 8:
		  chdef.Channel = ADC_CHANNEL_8;  //Voltage channel
		  break;
	  case 9:
		  chdef.Channel = ADC_CHANNEL_9; //Current channel
		  break;
	  case 11:
		  chdef.Channel = ADC_CHANNEL_11; //Water temperature channel
		  break;
	  case 12:
		  chdef.Channel = ADC_CHANNEL_12; //Ambient temperature channel
		  break;

	  }


	  chdef.Rank = 1;
	  chdef.SingleDiff = ADC_SINGLE_ENDED;
	  chdef.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
	  chdef.OffsetNumber = ADC_OFFSET_NONE;
	  chdef.Offset = 0;
	  HAL_ADC_ConfigChannel(&hadc1, &chdef);
	  HAL_ADC_Start(&hadc1);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
