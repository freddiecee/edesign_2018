/*
 * my_header.h
 *
 *  Created on: 20 Feb 2018
 *      Author: 19030827
 */

#include <stdbool.h>

#ifndef MY_HEADER_H_
#define MY_HEADER_H_

extern UART_HandleTypeDef huart1;
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;

extern uint8_t data[20]; //data buffer
extern uint8_t newtemp[10]; //holds set temperature data sent
extern uint8_t timeHold[13]; //Hold current set time
extern uint32_t adcValue[40]; // Get 20 Samples of the value

//Float arrays to hold ADC values
extern float currents[40];
extern float voltages[40];
extern float ambientTemp;
extern float waterTemp;

//Integers to hold count of Vrms, Irms digits
extern int countVRMS, countIRMS;
extern int channel;
extern int j;
extern unsigned adc_raw;

extern uint8_t m_voltRMS[6];
extern uint8_t m_ampRMS[5];

extern uint32_t adcCurrentValue[40];
extern uint32_t adcVoltageValue[40];

extern float Irms;
extern float Vrms;

extern char currentTemp[3];

//Interrupt flags
extern volatile bool adcTick;
extern volatile bool received;
extern volatile bool sysTickFlag;

extern int tempSize; //keeps track of the size of the temp entered as it's entered
extern int tempCount; //keeps temperature size after it has been entered

void initialiseUser(void);
void userloop(void);

void resolveData(void);
void returnStudentNo(void);
void setTemp(void);
void returnTemp(void);
void setTime(void);
void returnTime(void);
void writeToDigit(int digit, char character);

void sampleADC(void);
void telemetry(void);
void receivedata (void);

void switchElement(void);
void switchValve(void);
void detectFlowInput(void);

void adcstuff();
uint32_t maxValue(uint32_t values[]);

void calculateRMS();
float calcRMS(float* indata, int num);
void switchAdcChannel(int chann);
void dispStuff(void);

int toString(float value, int currentPos, int maxLength, uint8_t* stringData);

#endif /* MY_HEADER_H_ */
