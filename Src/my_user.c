/*
 * my_user.c
 *
 *  Created on: 20 Feb 2018
 *      Author: 19030827
 */


#include "main.h"
#include "stm32f3xx_hal.h"
#include "stm32f3xx_hal_gpio.h"
#include "my_header.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

extern TIM_HandleTypeDef htim2;

#define PULSE_DURATION 10
#define VALID_DURATION  5


uint8_t timeHold[13];

uint8_t teleData[30];
int countVRMS, countIRMS;

uint8_t m_voltRMS[6];
uint8_t m_ampRMS[5];

uint8_t m_voltRMS_a;
uint8_t m_ampRMS_a;

float currRMS;
float voltRMS;

int element = 1;
int valve = 1;

uint8_t flow1 = 0;
uint8_t flow0 = 0;

float flowCounter = 0;
int delayCounter = 40;

//Flag booleans
volatile bool adcTick;
volatile bool received;
volatile bool sysTickFlag;
bool valveOn = false;
bool elementOn = false;

//Holds the current state of the flow input
bool flowDetected = false;
//Determine if 10ms delay is active
bool delayFlow = false;

//Temperature stuff?
int tempCount = 1;
int counter = 1;
char currentTemp[3];

//initialise the user variables
void initialiseUser(void){
	adcTick = false;
	//valveOn = false;
	//elementOn = true;


	// start timer 2 for ADC sampling
	/*__HAL_TIM_ENABLE(&htim2);
	__HAL_TIM_ENABLE_IT(&htim2, TIM_IT_UPDATE);*/
	HAL_TIM_Base_Start_IT(&htim2);
}

//loop used to perform various functions on flags set
void userloop(void){
	//Check if adc value has been sampled
	if(adcTick){
		//sampleADC();

		/*if(!delayFlow) {

			delayFlow = detectFlowInput();

		}
		else {
			//Count down[Acts as the delay]
			delayCounter--;
			if(delayCounter == 0) {
				delayFlow = false;
				delayCounter = 40;
			}
		}*/



		adcTick = 0; //Reset the adc sampling flag
	}

	if(received){
		receivedata();
		//received = false;
	}

	if(sysTickFlag){
		dispStuff();
		sampleADC();
		detectFlowInput();
		sysTickFlag = 0;
	}
}

void dispStuff(void){
	  if (counter == 1) {

		  //writeToDigit(1, '0');
		  counter++;
	  }
	  else if (counter == 2)	  {

		  switch(tempCount){
		  case 8:
			  writeToDigit(2, currentTemp[0]);
			  break;
		  case 7:
			  //writeToDigit(2, currentTemp[0]);
			  break;
		  case 6:
			  //writeToDigit(2, currentTemp[0]);
			  break;
		  }


		  counter++;
	  }
	  else if (counter == 3)
	  {

		  switch(tempCount){
		  case 8:
			  writeToDigit(3, currentTemp[1]);
			  break;
		  case 7:
			  writeToDigit(3, currentTemp[0]);
			  break;
		  case 6:
			  break;
		  }

		  //writeToDigit(3, currentTemp[1] );
		  counter++;
	  }
	  else
	  {

		  switch(tempCount){
		  case 8:
			  writeToDigit(4, currentTemp[2]);
			  break;
		  case 7:
			  writeToDigit(4, currentTemp[1]);
			  break;
		  case 6:
			  writeToDigit(4, currentTemp[0]);
			  break;
		  }
		  //writeToDigit(4, currentTemp[2]);
		  counter = 1;
	  }
}

void resolveData(void)
{

	if (data[0] == '$')
	{
		switch (data[1])
		{

		case 'A':
			returnStudentNo();
			break;
		case 'B':
			switchValve();
			break;
		case 'D':
			switchElement();
			break;
		case 'F':
			setTemp();
			break;
		case 'G':
			returnTemp();
			break;
		case 'H':
			setTime();
			break;
		case 'K':
			telemetry();
			break;
		case 'I':
			returnTime();
			break;
		}

	}
}


//RETURNS THE STUDENT NUMBER
void returnStudentNo(void) {

	  data[0] = '$';
	  data[1] = 'A';
	  data[2] = ',';
	  data[3] = '1';
	  data[4] = '9';
	  data[5] = '0';
	  data[6] = '3';
	  data[7] = '0';
	  data[8] = '8';
	  data[9] = '2';
	  data[10] = '7';
	  data[11] = 0xD;
	  data[12] = 0xA;
	  HAL_UART_Transmit (&huart1, data, 13, 1000);
}

void switchValve(void){

	if (data[3] == '0') {
		//Switch valve off
		valveOn = false;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, 0);
	}
	else if (data[3] == '1')
	{
		//Switch valve on
		valveOn = true;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, 1);
	}

	  data[0] = '$';
	  data[1] = 'B';
	  data[2] = 0xD;
	  data[3] = 0xA;

	  HAL_UART_Transmit (&huart1, data, 4, 1000);
}

void switchElement(void){

	if (data[3] == '0') {
		//Switch element off
		elementOn = false;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 0);

	}
	else if (data[3] == '1')
	{
		//Switch element on
		elementOn = true;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 1);

	}

	  data[0] = '$';
	  data[1] = 'D';
	  data[2] = 0xD;
	  data[3] = 0xA;

	  HAL_UART_Transmit (&huart1, data, 4, 1000);
}

//SETS TEMPERATURE TO THAT ISSUED
void setTemp(void) {

	newtemp[0] = '$';
	newtemp[1] = 'G';
	newtemp[2] = ',';

	if (tempSize == 7) {
		newtemp[3] = data[3];
		newtemp[4] = data[4];
		newtemp[5] = data[5];
		newtemp[6] = 0xD;
		newtemp[7] = 0xA;
		tempCount = 8;

		currentTemp[0] = data[3];
		currentTemp[1] = data[4];
		currentTemp[2] = data[5];
	}
	else if (tempSize == 6) {
		newtemp[3] = data[3];
		newtemp[4] = data[4];
		newtemp[5] = 0xD;
		newtemp[6] = 0xA;
		tempCount = 7;

		currentTemp[0] = data[3];
		currentTemp[1] = data[4];
	}
	else {
		newtemp[3] = data[3];
		newtemp[4] = 0xD;
		newtemp[5] = 0xA;
		tempCount = 6;

		currentTemp[0] = data[3];
		currentTemp[1] = data[4];
	}

	 data[0] = '$';
	 data[1] = 'F';
	 data[2] = 0xD;
     data[3] = 0xA;
     data[4] = 0xA;
	HAL_UART_Transmit (&huart1, data, 4, 1000);

}

//RETURNS CURRENT SET TEMPERATURE
void returnTemp(void) {
	HAL_UART_Transmit (&huart1, newtemp, tempCount, 1000);
}

//SET THE TIME
void setTime(void) {
	int a;


	timeHold[0] = '$';
	timeHold[1] = 'I';

	for (a = 2; a < 11; a++) {
		timeHold[a] = data[a];
	}

	timeHold[11] = 0xD;
	timeHold[12] = 0xA;

    data[0] = '$';
	data[1] = 'H';
    data[2] = 0xD;
    data[3] = 0xA;
    data[4] = 0xA;

	HAL_UART_Transmit (&huart1, data, 4, 1000);

}

void telemetry (void) {



	countVRMS = 6; //max voltages characters
	countIRMS = 4; //Max no of characters to represent current

	/*m_voltRMS[0] = adcVoltageValue[0]/1000 + '0';
	m_voltRMS[1] = (adcVoltageValue[0]/100) - 10*(adcVoltageValue[0]/1000) + '0';
	m_voltRMS[2] = (adcVoltageValue[0]/10) - 10*(adcVoltageValue[0]/100) + '0';
	m_voltRMS[3] = (adcVoltageValue[0]%10) + '0';
	m_voltRMS[4] = '0';
	m_voltRMS[5] = '0';

	m_ampRMS[0] = adcCurrentValue[0]/1000 + '0';
	m_ampRMS[1] = (adcCurrentValue[0]/100) - 10*(adcCurrentValue[0]/1000) + '0';
	m_ampRMS[2] = (adcCurrentValue[0]/10) - 10*(adcCurrentValue[0]/100) + '0';
	m_ampRMS[3] = (adcCurrentValue[0]%10) + '0';
	m_ampRMS[4] = '0';*/

	float waterTempActual = ((3.3*((waterTemp - 750)/(4095 - 750))) / 0.01) - 25;
	float ambientTempActual = ((3.3*((ambientTemp - 750)/(4095 - 750))) / 0.01) - 34;
	float waterFlow = flowCounter * 100;

	int counter = 3;

	teleData[0] = '$';
	teleData[1] = 'K';
	teleData[2] = ',';

	//Use the toString function to convert rms values to characters
	counter = toString(Irms, counter, 10, teleData);

	/*for (int i = 0; i < countIRMS; i++) {
		teleData[counter] = m_ampRMS[i];
		//teleData[counter] = arrayCurr[i];
		counter++;
	}*/

	teleData[counter] = ',';
	counter++;

	//use toString function to convert Vrms to characters
	counter = toString(Vrms, counter, 10, teleData);

	/*for (int i = 0; i < countVRMS; i++) {
		teleData[counter] = m_voltRMS[i];
		//teleData[counter] = arrayVolt[i];
		counter++;
	}*/
	teleData[counter] = '0';
	teleData[counter + 1] = '0';
	counter += 2;


	teleData[counter] = ',';
	counter++;
	//teleData[counter + 1] = '0'; //Ambient temp here

	counter = toString(ambientTempActual, counter, 10, teleData);


	teleData[counter] = ',';
	counter++;
	//teleData[counter + 3] = '0'; //Water temp here

	counter = toString(waterTempActual, counter, 10, teleData);

	teleData[counter] = ',';
	counter++;
	//teleData[counter + 1] = '0'; //Water flow rate here

	counter = toString(waterFlow, counter, 10, teleData);

	teleData[counter] = ',';
	counter ++;

	//Do the on/off things here
	if (elementOn) {
		teleData[counter] = 'O';
		teleData[counter + 1] = 'N';
		teleData[counter + 2] = ',';
		counter += 3;
	}
	else
	{
		teleData[counter] = 'O';
		teleData[counter + 1] = 'F';
		teleData[counter + 2] = 'F';
		teleData[counter + 3] = ',';
		counter+= 4;
	}

	//Do the valve stuff here
	if(valveOn) {
		teleData[counter] = 'O';
		teleData[counter + 1] = 'P';
		teleData[counter + 2] = 'E';
		teleData[counter + 3] = 'N';
		counter += 4;
	}
	else
	{
		teleData[counter] = 'C';
		teleData[counter + 1] = 'L';
		teleData[counter + 2] = 'O';
		teleData[counter + 3] = 'S';
		teleData[counter + 4] = 'E';
		teleData[counter + 5] = 'D';
		counter += 6;
	}

	teleData[counter] = 0xD;
	teleData[counter + 1] = 0xA;
	counter += 2;

	HAL_UART_Transmit (&huart1, teleData, counter, 1000);
}

//RETURN THE TIME
void returnTime(void) {
	HAL_UART_Transmit (&huart1, timeHold, 13, 1000);
}

void writeToDigit(int digit, char character) {

	switch (character) {
	case '1':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 1);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 1);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);  //e - 5
		break;
	case '2':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 1);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 0);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);  //e - 5
		break;
	case '3':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 0);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);  //e - 5
		break;
	case '4':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 1);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);  //e - 5
		break;
	case '5':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 1);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 0);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);  //e - 5
		break;
	case '6':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 1);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 0);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);  //e - 5
		break;
	case '7':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 1);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 1);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);  //e - 5
		break;
	case '8':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 0);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);  //e - 5
		break;
	case '9':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 0);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);  //e - 5
		break;
	case '0':
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 1);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 0);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);  //e - 5
		break;
	default:
		//Writes an 'X'
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);  //a - 1
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);  //b - 2
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);  //c - 3
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);  //f - 7
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);  //g - 6
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, 1);  //d - 4
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);  //e - 5
		break;
	}

	//Select digit to write to
	switch (digit) {
	case 1:
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 0); //Digit 1
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 1); //Digit 2
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 1); //Digit 3
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 1); //Digit 4
		break;
	case 2:
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 1); //Digit 1
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 0); //Digit 2
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 1); //Digit 3
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 1); //Digit 4
		break;
	case 3:
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 1); //Digit 1
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 1); //Digit 2
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0); //Digit 3
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 1); //Digit 4
		break;
	case 4:
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 1); //Digit 1
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 1); //Digit 2
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 1); //Digit 3
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 0); //Digit 4
		break;
	default:
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 1); //Digit 1
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 0); //Digit 2
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 1); //Digit 3
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 1); //Digit 4
		  break;
	}

}

uint32_t maxValue(uint32_t values[]) {

    uint32_t hold = 0;

    for (int i = 0; i < 20 ; i++) {
    	if(values[i] > hold) {
    		hold = values[i];
    	}
    }

	return hold;
}

/*void calculateRMS() {
	m_voltRMS_a = maxValue(adcVoltageValue)/sqrt(2);
	m_ampRMS_a = maxValue(adcCurrentValue)/sqrt(2);
}*/

float calcRMS(float* indata, int num) {
    float xrms = 0;
    float sum = 0;

    for(int i = 0; i <num; i++){
        sum += indata[i] * indata[i];
    }

    sum = sum/num;
    xrms = sqrt(sum);
    return xrms;
}

int toString(float value, int currentPos, int maxLength, uint8_t* stringData) {
	int digits;
	int count = currentPos;

	//Check number of digits to be converted
	if (value < 10) {
		digits = 1;
	}
	else if(value < 100) {
		digits = 2;
	}
	else if(value < 1000) {
		digits = 3;
	} else if(value < 10000){
		digits = 4;
	}
	else if(value < 100000){
		digits = 5;
	}
	else if(value < 1000000){
		digits = 6;
	}
	else if (value < 10000000){
		digits = 7;
	}
	else {
		digits = 8;
	}


	if (digits > maxLength) {
		return 'a';
	}

	int position = digits;

	while (position > 0) {
		//add value to respective position in array
		stringData[currentPos + (position - 1)] = (int) value % 10 + '0';

		value /= 10;
		position--;
		count++;

	}

	return count; //return the current position of the array


}

//Get's called every 250ns when the timer 2 period elapses
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim2) {
		adcTick = true;
	}
}

//Need a 10ms delay for the water flow counter
void delayPeriod(){

}

//Check for a high
void detectFlowInput(void) {

	GPIO_PinState pinState = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15);

	//If there is a high edge
	if (pinState == 0) {
		//Delay for 10ns before reading again
		//flowCounter++; //Increment flow count[Each flow count represents 100ml]

		flow1++;
		flow0=0;

		if (flow1 >= 5) { //Test for Flow Indicator

			flowCounter++;
			flow1 = 0;

		}

	}
	else {
		flow0++;
		flow1 = 0;

	}

}



